import redis
import unittest
import markov
from markov import Markov



test_data =  Markov(prefix="test")
test_data.flush(prefix=True)

line = ['some', 'words']
test_data.add_line_to_index(line)
line = ['words', 'that']
test_data.add_line_to_index(line)
line = ['that', 'you']
test_data.add_line_to_index(line)
line = ['you', 'would']
test_data.add_line_to_index(line)
line = ['would', 'like']
test_data.add_line_to_index(line)
line = ['like', 'to']
test_data.add_line_to_index(line)
line = ['to', 'remove']
test_data.add_line_to_index(line)



line = ['some', 'words', 'that', 'you', 'would', 'like', 'to', 'remove']
#line = ['to', 'remove']
print test_data.score_for_line(line)

'''
line = ['some', 'words', 'that', 'you', 'would', 'like', 'to', 'remove']
test_data.add_line_to_index(line)
print test_data.score_for_line(line)
'''


'''
line = ['some']
test_data.add_line_to_index(line)
line = ['words']
test_data.add_line_to_index(line)
line = ['that']
test_data.add_line_to_index(line)
line = ['you']
test_data.add_line_to_index(line)
line = ['would']
test_data.add_line_to_index(line)
'''


new_line = test_data.generate(max_words=8)
print new_line

#new_line = test_data.generate(seed=['some', 'words', 'that'], max_words=8)
#print new_line

#test_data.flush(prefix="yes")



