################################################################################
# (c) 2013, COmputer SEcurity (COSEC) - Universidad Carlos III de Madrid
# Author: Guillermo Suarez de Tangil - guillermo.suarez.tangil@uc3m.es
# Based on Droidbox - (c) 2011, The Honeynet Project 
################################################################################

import sys, json, time, curses, signal, os
import zipfile, StringIO
from array import array
from threading import Thread
from xml.dom import minidom
import hashlib
from pylab import *
from yaml import load, dump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper
import re
import redis
import unittest
import markov
from markov.markov import Markov

from device2cloneEvents.LogcatEvent import *

import droidbox
from droidbox.cosec_droidbox import *

# -----------------------
# Constants
# -----------------------
debug = 1

# -----------------------
# Params
# -----------------------


duration = 0
if len(sys.argv) > 1 and len(sys.argv[1]) > 0:
    try:
        duration = int(sys.argv[1])
    except:
	    print "Usage: ./cloud_monitoring.sh [duration]"
	    sys.exit(1)


# -----------------------
# Logo
# -----------------------


print '##########################################################################'
print '# Author: Guillermo Suarez de Tangil - guillermo.suarez.tangil@uc3m.es   #'
print '# (c) 2013, COmputer SEcurity (COSEC) - Universidad Carlos III de Madrid #'
print '##########################################################################'
print '#                                                                        #'
print '#                                                                        #'
print '#                                                                        #'
print '#   .d8888b .d88b.  .d8888b   .d88b.   .d8888b                           #'
print '#  d88P"   d88""88b 88K      d8P  Y8b d88P"                              #'
print '#  888     888  888 "Y8888b. 88888888 888                                #'
print '#  Y88b.   Y88..88P      X88 Y8b.     Y88b.                              #'
print '#   "Y8888P "Y88P"   88888P\'  "Y8888   "Y8888P                           #'
print '#                                                                        #'
print '#                                                                        #'
print '#                                                                        #'
print '#                    .d8888b.                                            #'
print '#                   d88P  Y88b                                           #'
print '#                        .d88P                                           #'
print '#  888  888  .d8888b    8888"  88888b.d88b.       .d88b.  .d8888b        #'
print '#  888  888 d88P"        "Y8b. 888 "888 "88b     d8P  Y8b 88K            #'
print '#  888  888 888     888    888 888  888  888     88888888 "Y8888b.       #'
print '#  Y88b 888 Y88b.   Y88b  d88P 888  888  888 d8b Y8b.          X88       #'
print '#   "Y88888  "Y8888P "Y8888P"  888  888  888 Y8P  "Y8888   88888P\'       #'
print '#                                                                        #'
print '#                                                                        #'
print '#                                                                        #'
print '##########################################################################'
print '######################### COSEC Cloud2Behaviour ##########################'
print '##########################################################################'


# -----------------------
# Main
# -----------------------


print "---- running DroidBox ----"
droidbox = DroidBox(duration)
signature = droidbox.run(sys.stdin)
print signature




