#!/bin/bash

if [ ! -z "$1"  ];
then
    EMULATOR="$1"
else
    EMULATOR="clonecloud23"
fi

#emulator -avd $EMULATOR -prop dalvik.vm.execution-mode=int:portable &
#emulator -avd $EMULATOR -system imagesDroidbox/system.img -ramdisk imagesDroidbox/ramdisk.img -kernel imagesDroidbox/zImage -wipe-data -prop dalvik.vm.execution-mode=int:portable &
emulator -avd $EMULATOR -system imagesDroidbox/system.img -ramdisk imagesDroidbox/ramdisk.img -kernel imagesDroidbox/zImage -prop dalvik.vm.execution-mode=int:portable &
