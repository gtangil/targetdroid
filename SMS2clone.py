
import sys
import telnetlib, socket


print '##########################################################################'
print '# Author: Guillermo Suarez de Tangil - guillermo.suarez.tangil@uc3m.es   #'
print '# (c) 2013, COmputer SEcurity (COSEC) - Universidad Carlos III de Madrid #'
print '##########################################################################'
print '#                                                                        #'
print '#                                                                        #'
print '#                                                                        #'
print '#   .d8888b .d88b.  .d8888b   .d88b.   .d8888b                           #'
print '#  d88P"   d88""88b 88K      d8P  Y8b d88P"                              #'
print '#  888     888  888 "Y8888b. 88888888 888                                #'
print '#  Y88b.   Y88..88P      X88 Y8b.     Y88b.                              #'
print '#   "Y8888P "Y88P"   88888P\'  "Y8888   "Y8888P                           #'
print '#                                                                        #'
print '#                                                                        #'
print '#                                                                        #'
print '#                    .d8888b.                                            #'
print '#                   d88P  Y88b                                           #'
print '#                        .d88P                                           #'
print '#  888  888  .d8888b    8888"  88888b.d88b.       .d88b.  .d8888b        #'
print '#  888  888 d88P"        "Y8b. 888 "888 "88b     d8P  Y8b 88K            #'
print '#  888  888 888     888    888 888  888  888     88888888 "Y8888b.       #'
print '#  Y88b 888 Y88b.   Y88b  d88P 888  888  888 d8b Y8b.          X88       #'
print '#   "Y88888  "Y8888P "Y8888P"  888  888  888 Y8P  "Y8888   88888P\'       #'
print '#                                                                        #'
print '#                                                                        #'
print '#                                                                        #'
print '##########################################################################'
print '########################### COSEC SMS2Cloud ############################'
print '##########################################################################'



ip = "localhost"
port = 5554

telef = ""
sms = ""
if len(sys.argv) <= 2:
    print "Usage: SMS2Cloud telf_number message"
    sys.exit(1)
if len(sys.argv) > 1 and len(sys.argv[1]) > 0:
    telef = sys.argv[1]
if len(sys.argv) > 2 and len(sys.argv[2]) > 0:
    sms = sys.argv[2]


if len(sys.argv) <= 2:
    print "Usage: SMS2Cloud telf_number message"
    sys.exit(1)

try:
    telnet = telnetlib.Telnet(ip, port)
except EOFError:
    print "Error: cannot connect to the cloud..."

print "Sending SMS to the clone cloud -> " + telef + ": " + sms 

# read until it is ready to interact
telnet.read_until("OK")
telnet.write("sms send " + telef + " " + sms + "\n")

telnet.close()
