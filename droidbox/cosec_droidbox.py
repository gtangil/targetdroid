################################################################################
# (c) 2013, COmputer SEcurity (COSEC) - Universidad Carlos III de Madrid
# Author: Guillermo Suarez de Tangil - guillermo.suarez.tangil@uc3m.es
#
# Based on Droidbox:
# (c) 2011, The Honeynet Project
# Author: Patrik Lantz patrik@pjlantz.com
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
#
################################################################################

import sys, json, time, curses, signal, os
import zipfile, StringIO
from threading import Thread
from xml.dom import minidom
from subprocess import call, PIPE
from utils import AXMLPrinter
import hashlib
from pylab import *
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from matplotlib.font_manager import FontProperties

import copy

sendsms = {}
phonecalls = {}
cryptousage = {}
netbuffer = {}

dexclass = {}
dataleaks = {}
opennet = {}
sendnet = {}
recvnet = {}
fdaccess = {}
servicestart = {}
xml = {}
udpConn = []
permissions = []
activities = []
activityaction = {}
enfperm = []
packageNames = []
recvs = []
recvsaction = {}
accessedfiles = {}

tags = { 0x1 :   "TAINT_LOCATION",      0x2: "TAINT_CONTACTS",        0x4: "TAINT_MIC",            0x8: "TAINT_PHONE_NUMBER",
    0x10:   "TAINT_LOCATION_GPS",  0x20: "TAINT_LOCATION_NET",   0x40: "TAINT_LOCATION_LAST", 0x80: "TAINT_CAMERA",
    0x100:  "TAINT_ACCELEROMETER", 0x200: "TAINT_SMS",           0x400: "TAINT_IMEI",         0x800: "TAINT_IMSI",
    0x1000: "TAINT_ICCID",         0x2000: "TAINT_DEVICE_SN",    0x4000: "TAINT_ACCOUNT",     0x8000: "TAINT_BROWSER",
    0x10000: "TAINT_OTHERDB",      0x20000: "TAINT_FILECONTENT", 0x40000: "TAINT_PACKAGE",    0x80000: "TAINT_CALL_LOG",
    0x100000: "TAINT_EMAIL",       0x200000: "TAINT_CALENDAR",   0x400000: "TAINT_SETTINGS" }


class CountingThread(Thread):
    """
    Used for user interface, showing in progress sign 
    and number of collected logs from the sandbox system
    """

    def __init__ (self):
        """
        Constructor
        """
        
        Thread.__init__(self)
        self.stop = False
        self.logs = 0
        
    def stopCounting(self):
        """
        Mark to stop this thread 
        """
        
        self.stop = True
        
    def increaseCount(self):
        
        self.logs = self.logs + 1

    def run(self):
        """
        Update the progress sign and 
        number of collected logs
        """
        
        signs = ['|', '/', '-', '\\']
        counter = 0
        while 1:
            sign = signs[counter % len(signs)]
            time.sleep(0.5)
            counter = counter + 1
            if self.stop:
                break


class DroidBox(object):
        
    def __init__ (self, duration):
        # ----------------------- Logo ----------------------- 
        print " ____                        __  ____"
        print "/\  _`\               __    /\ \/\  _`\\"
        print "\ \ \/\ \  _ __  ___ /\_\   \_\ \ \ \L\ \   ___   __  _"
        print " \ \ \ \ \/\`'__\ __`\/\ \  /'_` \ \  _ <' / __`\/\ \/'\\"
        print "  \ \ \_\ \ \ \/\ \L\ \ \ \/\ \L\ \ \ \L\ \\ \L\ \/>  </"
        print "   \ \____/\ \_\ \____/\ \_\ \___,_\ \____/ \____//\_/\_\\"
        print "    \/___/  \/_/\/___/  \/_/\/__,_ /\/___/ \/___/ \//\/_/"

        
        # ----------------------- Input -----------------------

        self.duration = duration


    def fileHash(self, f, block_size=2**8):
        """
        Calculate MD5,SHA-1, SHA-256
        hashes of APK input file
        """
        
        md5 = hashlib.md5()
        sha1 = hashlib.sha1()
        sha256 = hashlib.sha256()
        f = open(f, 'rb')
        while True:
            data = f.read(block_size)
            if not data:
                break
            md5.update(data)
            sha1.update(data)
            sha256.update(data)
        return [md5.hexdigest(), sha1.hexdigest(), sha256.hexdigest()]
        
    def hexToStr(self, hexStr):
        """
        Convert a string hex byte values into a byte string
        """
     
        bytes = []
        hexStr = ''.join(hexStr.split(" "))
        for i in range(0, len(hexStr), 2):
            bytes.append(chr(int(hexStr[i:i+2], 16)))
        return ''.join( bytes )
        
    def decode(self, s, encodings=('ascii', 'utf8', 'latin1')):
        for encoding in encodings:
            try:
                return s.decode(encoding)
            except UnicodeDecodeError:
                pass
        return s.decode('ascii', 'ignore')

    def getTags(self, tagParam):
        """
        Retrieve the tag names found within a tag
        """
        
        tagsFound = []
        for tag in tags.keys():
            if tagParam & tag != 0:
                tagsFound.append(tags[tag])
        return tagsFound

    def interruptHandler(self, signum, frame):
        """ 
        Raise interrupt for the blocking call 'logcatInput = sys.stdin.readline()'
        
        """
        raise KeyboardInterrupt	

   
    def run(self, input):
        
        #count = CountingThread()
        #count.start()
        
        self.timestamp = time.time()
        if self.duration and self.duration > 0:
            signal.signal(signal.SIGALRM, self.interruptHandler)
            signal.alarm(self.duration)
        while 1:
            try:
                logcatInput = input.readline() 
                
                if not logcatInput:
                    break
                boxlog = logcatInput.split('DroidBox:')
                if len(boxlog) > 1:
                
                    #print logcatInput
                    
                    try:
                        load = json.loads(self.decode(boxlog[1]))
                        # DexClassLoader
                        if load.has_key('DexClassLoader'):
                            load['DexClassLoader']['type'] = 'dexload'
                            dexclass[time.time() - self.timestamp] = load['DexClassLoader']
                            #count.increaseCount()
                        # service started
                        if load.has_key('ServiceStart'):
                            load['ServiceStart']['type'] = 'service'
                            servicestart[time.time() - self.timestamp] = load['ServiceStart']
                            #count.increaseCount()
                        # received data from net
                        if load.has_key('RecvNet'):   
                            host = load['RecvNet']['srchost']
                            port = load['RecvNet']['srcport']
                            if load['RecvNet'].has_key('type') and load['RecvNet']['type'] == 'UDP':
                                recvdata = {'type': 'net-read', 'host': host, 'port': port, 'data': load['RecvNet']['data']}
                                recvnet[time.time() - self.timestamp] = recvdata
                                #count.increaseCount()
                            else:
                                tmp_load = load['RecvNet']['fd']
                                hostport = host + ":" + port + ":" + tmp_load
                                if netbuffer.has_key(hostport):
                                    if len(netbuffer[hostport]) == 0:
                                        netbuffer[hostport] = str(time.time()-self.timestamp) + ":"
                                    netbuffer[hostport] =  netbuffer[hostport] + load['RecvNet']['data']
                        # fdaccess
                        if load.has_key('FdAccess'):
                            accessedfiles[load['FdAccess']['id']] = load['FdAccess']['path']
                        # file read or write     
                        if load.has_key('FileRW'):
                            if accessedfiles.has_key(load['FileRW']['id']) and not "/dev/pts" in accessedfiles[load['FileRW']['id']]:
                                load['FileRW']['path'] = accessedfiles[load['FileRW']['id']]
                                if load['FileRW']['operation'] == 'write':
                                    load['FileRW']['type'] = 'file-write'
                                else:
                                    load['FileRW']['type'] = 'file-read'
                                fdaccess[time.time()-self.timestamp] = load['FileRW']
                                #count.increaseCount()
                        # opened network connection log
                        if load.has_key('OpenNet'):
                            if load['OpenNet'].has_key('type') and load['OpenNet']['type'] == 'UDP':
                                opennet[time.time()-self.timestamp] = load['OpenNet']
                                ref = load['OpenNet']['desthost'] + load['OpenNet']['destport']
                                if ref not in udpConn:
                                    udpConn.append(ref)
                            else:
                                load['OpenNet']['type'] = 'net-open'
                                opennet[time.time()-self.timestamp] = load['OpenNet']
                                host = load['OpenNet']['desthost']
                                port = load['OpenNet']['destport']
                                tmp_net = load['OpenNet']['fd']
                                netbuffer[host + ":" + port + ":" + tmp_net] = ""
                            #count.increaseCount()
                        # closed socket
                        if load.has_key('CloseNet'):
                            host = load['CloseNet']['desthost']
                            port = load['CloseNet']['destport']
                            ref = host + ":" + port
                            if ref not in udpConn:
                                tmp_net = load['CloseNet']['fd']
                                try:
                                    data = netbuffer[host + ":" + port + ":" + tmp_net]
                                except KeyError:
                                    continue
                                stamp = float(data.split(":")[0])
                                buffer = data.split(":")[1]
                                recvdata =  { 'type': 'net read', 'host': host, 'port': port, 'data': buffer}
                                recvnet[stamp] = recvdata
                                netbuffer[host + ":" + port + ":" + tmp_net] = ""
                                #count.increaseCount()
                            else:
                                ref.remove(ref)
                        # outgoing network activity log
                        if load.has_key('SendNet'):
                            if load['SendNet'].has_key('type') and load['SendNet']['type'] == 'UDP':
                                ref = load['SendNet']['desthost'] + load['SendNet']['destport']
                                if ref not in udpConn:
                                    udpConn.append(ref)
                                    opennet[time.time()-self.timestamp] = load['SendNet']
                            load['SendNet']['type'] = 'net-write'
                            sendnet[time.time()-self.timestamp] = load['SendNet']
                            #count.increaseCount()                                          
                        # data leak log
                        if load.has_key('DataLeak'):                   
                            if load['DataLeak']['sink'] == 'File':
                                if accessedfiles.has_key(load['DataLeak']['id']):
                                    load['DataLeak']['path'] = accessedfiles[load['DataLeak']['id']]          
                            load['DataLeak']['type'] = 'leak'
                            dataleaks[time.time()-self.timestamp] = load['DataLeak']
                            #count.increaseCount()
                        # sent sms log
                        if load.has_key('SendSMS'):
                            load['SendSMS']['type'] = 'sms'
                            sendsms[time.time()-self.timestamp] = load['SendSMS']
                            #count.increaseCount()
                        # phone call log
                        if load.has_key('PhoneCall'):
                            load['PhoneCall']['type'] = 'call'
                            phonecalls[time.time()-self.timestamp] = load['PhoneCall']
                            #count.increaseCount()
                        # crypto api usage log
                        if load.has_key('CryptoUsage'):
                            load['CryptoUsage']['type'] = 'crypto'                                                                   
                            cryptousage[time.time()-self.timestamp] = load['CryptoUsage']
                            #count.increaseCount()
                    except ValueError:
                        pass

            except KeyboardInterrupt:  
                # Wait for counting thread to stop
                #count.stopCounting()
                #count.join()
                break

    
        signature = list()
        mergedLogs = dict(dexclass.items() + servicestart.items() + phonecalls.items() + sendsms.items() + recvnet.items() +
                          dataleaks.items() + cryptousage.items() + opennet.items() + sendnet.items() + fdaccess.items())
        keys = mergedLogs.keys()
        keys.sort()
        for key in keys:
            temp = mergedLogs[key]
            signature.append(temp['type'])
        
        return signature
    
