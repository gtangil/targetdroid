package es.uc3m.cosec.targetdroid;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

public class Receiver extends BroadcastReceiver {
	
	final String TAG = "CAPTUREVENTS";
	final String TAGCALL = "TAGCALL";
	final String TAGSMS = "TAGSMS";
	final String TAGRINGER = "TAGRINGER";
	final String TAGTIME = "TAGTIME";
	final String TAGPICTURE = "TAGPICTURE";
	final String TAGVIDEO = "TAGVIDEO";
	final String TAGSHUTDOWN = "TAGSHUTDOWN";
	final String TAGWALLPAPER = "TAGWALLPAPER";
	
	public Receiver() {
	}

	@Override
	public void onReceive(Context context, Intent intent) {

		switch(intent.getAction()){
		case "es.uc3m.cosec.targetdroid.BOOT_COMPLETED":
			Log.d(TAG , "TEST");
		case Intent.ACTION_BOOT_COMPLETED:
			// Start service CaptureEvents
			Intent myIntent = new Intent(context, CaptureEvents.class);
			context.startService(myIntent);
			break;
		case "android.intent.action.NEW_OUTGOING_CALL":
			// log outgoing call with number
			String number = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
			Log.d(TAG , TAGCALL + " | OUTGOING CALL: " + number);
			break;
		case "android.provider.Telephony.SMS_RECEIVED":
			// log incoming sms
            Bundle bundle = intent.getExtras();
            SmsMessage[] msgs = null;
            String msg_from = "";
            String msgBody = "";
            if (bundle != null){
                try{
                    Object[] pdus = (Object[]) bundle.get("pdus");
                    msgs = new SmsMessage[pdus.length];
                    for(int i=0; i<msgs.length; i++){
                        msgs[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
                        msg_from = msg_from + msgs[i].getOriginatingAddress();
                        msgBody = msgBody + msgs[i].getMessageBody();
                    }
                }catch(Exception e){
                	Log.d(TAG, "Exception: " + e.getMessage());
                }
                Log.d(TAG , TAGSMS + " | INCOMING SMS | number: " + msg_from + " | body: " + msgBody.replace('\n', ' ') );
            }
			break;
		case "android.media.VOLUME_CHANGED_ACTION":
			// code = 5 | volume changed
			Log.d(TAG , TAGRINGER + " | VOLUME: " + intent.getIntExtra("android.media.EXTRA_VOLUME_STREAM_VALUE", -1));
			break;
		case "android.media.RINGER_MODE_CHANGED":
			Log.d(TAG , TAGRINGER+ " | MODE: " + intent.getIntExtra("android.media.EXTRA_RINGER_MODE", -1));
			// ringer = 0 -> Silence with no vibration
			// ringer = 1 -> Vibrator mode
			// ringer = 2 -> Normal mode
			break;
		case Intent.ACTION_TIME_CHANGED:
			// new time has been set
			/*
			 * This code works in case of using the replicator with code = 12 | Set Time, but it's not a system app
			//Date fecha = new Date();
			long fechaDate = fecha.getTime();
			Log.d(TAG , TAGTIME+" | NEWTIME:"+fechaDate);*/

			Date fecha = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd.hhmmss");
			Log.d(TAG , TAGTIME+" | NEWTIME:"+sdf.format(fecha));
			
			break;
		case Intent.ACTION_TIMEZONE_CHANGED:
			// time zone has been changed
			Log.d(TAG , TAGTIME+" | TIMEZONE:"+intent.getStringExtra("time-zone"));
			break;
		case "android.hardware.action.NEW_PICTURE":
			Log.d(TAG , TAGPICTURE+" | NEWPICTURE");
			break;
		case "com.android.camera.NEW_PICTURE":
			Log.d(TAG , TAGPICTURE+" | NEWPICTURE");
			break;
		case Intent.ACTION_SHUTDOWN:
			// ThreadShutdown started
			Log.d(TAG , TAGSHUTDOWN);
			break;
		case "android.intent.action.WALLPAPER_CHANGED":
			Log.d(TAG , TAGWALLPAPER);
			break;
		default:
			break;
		}
	}
}
