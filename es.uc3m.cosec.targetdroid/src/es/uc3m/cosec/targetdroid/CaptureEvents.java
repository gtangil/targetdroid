package es.uc3m.cosec.targetdroid;

import java.util.Arrays;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Toast;

public class CaptureEvents extends Service {
	
	private LocationManager locationManager = null;
	private LocationListener locationListener = null;	
	
	final String TAG = "CAPTUREVENTS";
	final String TAGLOCATION = "TAGLOCATION";
	final String TAGSENSORS = "TAGSENSORS";
	final String TAGCALL = "TAGCALL";
	final String TAGSMS = "TAGSMS";
	final String TAGCONTACT = "TAGCONTACT";
	final String TAGSCREEN = "TAGSCREEN";
	final String TAGBATTERY = "TAGBATTERY";
	
	private SensorManager mSensorManager;
	private Sensor mSensorAccelerometer, mSensorGravity, mSensorGyroscope, mSensorRotation;
    
	static long count = 0;
	static double init_time = 0;
	
    static String messageId="";
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
			
		Toast.makeText(CaptureEvents.this, "SERVICIO INICIADO ON BOOT", Toast.LENGTH_SHORT).show();
		
		// --- sensors ---
		
	    mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		mSensorAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		mSensorGravity = mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
		mSensorGyroscope = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
		mSensorRotation = mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
	    
		SensorEventListener mSensorEventListener = new SensorEventListener(){
			
			public void onSensorChanged(SensorEvent event) {
			
				if(count==0){
					init_time = System.currentTimeMillis();
			    }
			    count++;
			    if(count%5==0){
			    	double num = Math.random()*100;
			    	if(Math.ceil(num) < 1.0){
			    		try{
						    Log.d(TAG, TAGSENSORS + " | " + event.sensor.getType() + " | " + event.sensor.getName() + " | " + Arrays.toString(event.values)); 
					    }catch(ArithmeticException e){
					    	Log.d(TAG, TAGSENSORS + " | exception " + count);
					    }
			    	}
			    }
			}

			@Override
			public void onAccuracyChanged(Sensor sensor, int accuracy) {
				// TODO Auto-generated method stub
			}
		};

//		mSensorManager.registerListener(mSensorEventListener, mSensorAccelerometer, SensorManager.SENSOR_DELAY_GAME);
//		mSensorManager.registerListener(mSensorEventListener, mSensorGravity, SensorManager.SENSOR_DELAY_GAME);
//		mSensorManager.registerListener(mSensorEventListener, mSensorGyroscope, SensorManager.SENSOR_DELAY_GAME);
//		mSensorManager.registerListener(mSensorEventListener, mSensorRotation, SensorManager.SENSOR_DELAY_GAME);
		
		// --- location ---
		
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		
		boolean disponible = false;
		
		try{
			disponible = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		}catch(Exception e){
			Log.d(TAG, TAGLOCATION + " Exception: " + e.toString());
		}
		
		if (!disponible) Toast.makeText(this, "GPS required but not active. Turn it ON", Toast.LENGTH_LONG).show();
		
		locationListener = new LocationListener(){

			@Override
			public void onLocationChanged(Location location) {
				// TODO Auto-generated method stub
				Log.d(TAG , TAGLOCATION + " | Lat:" + location.getLatitude() + " | Long:" + location.getLongitude());
			}

			@Override
			public void onStatusChanged(String provider, int status,
					Bundle extras) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onProviderEnabled(String provider) {
				// TODO Auto-generated method stub
				Toast.makeText(CaptureEvents.this, "GPS Enabled", Toast.LENGTH_LONG).show();
			}

			@Override
			public void onProviderDisabled(String provider) {
				// TODO Auto-generated method stub
				Toast.makeText(CaptureEvents.this, "GPS disabled", Toast.LENGTH_LONG).show();
			}
			
		};
		
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
		
		// --- registering incoming phone calls ---
		
		PhoneStateListener mPhoneListener = new PhoneStateListener() {
			public void onCallStateChanged (int state, String incomingNumber) {
			         try {
			             switch (state) {
			             case TelephonyManager.CALL_STATE_RINGING:
			                 //Toast.makeText(CaptureEvents.this, "CALL_STATE_RINGING: ", Toast.LENGTH_SHORT).show();
			            	 Log.d(TAG , TAGCALL + " | RINGING: " + incomingNumber);
			                 break;
			             case TelephonyManager.CALL_STATE_OFFHOOK:
			                 //Toast.makeText(CaptureEvents.this, "CALL_STATE_OFFHOOK", Toast.LENGTH_SHORT).show();
			                 Log.d(TAG , TAGCALL + " | OFFHOOK: " + incomingNumber);
			                 break;
			             case TelephonyManager.CALL_STATE_IDLE:
			                 //Toast.makeText(CaptureEvents.this, "CALL_STATE_IDLE", Toast.LENGTH_SHORT).show();
			                 Log.d(TAG , TAGCALL + " | IDLE: " + incomingNumber);
			                 break;
			             default:
			                 //Toast.makeText(CaptureEvents.this, "default", Toast.LENGTH_SHORT).show();
			                 //Log.i("Default", "Unknown phone state=" + state);
			            	 Log.d(TAG , TAGCALL + " | Unknown phone state ");
			             }
			        } catch (Exception e) {
			            //Log.i("Exception", "PhoneStateListener() e = " + e);
			        	Log.d(TAG , TAGCALL + " | phone state listener EXCEPTION");
			        }
			    }
			};
			
		TelephonyManager mTM = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
		mTM.listen(mPhoneListener, PhoneStateListener.LISTEN_CALL_STATE);
		
		// --- content resolver listeners ---
		
		ContentResolver contentResolver = this.getApplicationContext().getContentResolver();
		
		// listen to sended SMS
		MyObserver observer = new MyObserver(new Handler(), this, android.provider.Telephony.Sms.CONTENT_URI.toString());
		contentResolver.registerContentObserver(android.provider.Telephony.Sms.CONTENT_URI,true, observer);
		
		// listen to contact modified (not implemented)
		MyObserver observer2 = new MyObserver(new Handler(), this, ContactsContract.Contacts.CONTENT_URI.toString());
		contentResolver.registerContentObserver(ContactsContract.Contacts.CONTENT_URI,true, observer2);
		
		// --- catch screen off and on. Not available in manifest, must be registered hereby ---
		
		BroadcastReceiver receiver = new BroadcastReceiver(){

			@Override
			public void onReceive(Context context, Intent intent) {
				// TODO Auto-generated method stub
				switch(intent.getAction()){
				case "android.intent.action.SCREEN_OFF":
					Log.d(TAG , TAGSCREEN + " | OFF");
					break;
				case "android.intent.action.SCREEN_ON":
					Log.d(TAG , TAGSCREEN + " | ON"); 
					break;
				}
			}
			
		};
		registerReceiver(receiver, new IntentFilter(Intent.ACTION_SCREEN_OFF));
		registerReceiver(receiver, new IntentFilter(Intent.ACTION_SCREEN_ON));
		
		// --- Battery Receiver, replicate with telnet ---
		
		BroadcastReceiver batteryReceiver = new BroadcastReceiver() {
            int level = -2;
            int plugged = -2;
            int status = -2;
            @Override
            public void onReceive(Context context, Intent intent) {
                level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
                plugged = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
                status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
                Log.d(TAG , TAGBATTERY + " | LEVEL:"+String.valueOf(level) + " | PLUGGED:"+String.valueOf(plugged) + " | STATUS:"+String.valueOf(status));
                // plugged:1 = AC | plugged:2 = USB | plugged:0 = unplugged (on battery)
                // status:3 = descargando | status:2 = cargando | status:5 = FULL
            }
        };
        IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        registerReceiver(batteryReceiver, filter);
        

        
		// fin service
	return Service.START_STICKY;
	}
	
    
	@Override
	public IBinder onBind(Intent intent) {
		// TODO: Return the communication channel to the service.
		throw new UnsupportedOperationException("Not yet implemented");
	}
	
	
    
}
