package es.uc3m.cosec.targetdroid;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Handler;
import android.provider.ContactsContract;
import android.util.Log;

public class MyObserver extends ContentObserver {
	
	final String TAG = "CAPTUREVENTS";
	final String TAGSMS = "TAGSMS";
	final String TAGCONTACT = "TAGCONTACT";
	
	String content;
	Context contexto;
	
	public MyObserver(Handler handler, Context contexto, String content) {
		super(handler);
		this.contexto = contexto;
		this.content = content;
	}
	
	@Override
	public void onChange(boolean selfChange) {
		super.onChange(selfChange);
		Cursor cur;
		if (android.provider.Telephony.Sms.CONTENT_URI.toString().compareTo(content)==0) {
			// catch and log outgoing sms
			try{
				cur = contexto.getContentResolver().query(android.provider.Telephony.Sms.CONTENT_URI, null, null, null, null);
				if (cur.moveToFirst()) {
					String body = cur.getString(
		                    cur.getColumnIndexOrThrow("body")).toString();
					String address = cur.getString(
		                    cur.getColumnIndexOrThrow("address")).toString();
					// sms.type = 4 implies outgoing SMS
					if(cur.getString(cur.getColumnIndexOrThrow("type")).toString().compareTo("4") == 0){
						Log.d(TAG , TAGSMS + " | OUTGOING SMS | number: " + address + " | body: " + body.replace('\n', ' '));
					}
				}
			}catch(Exception e){
				Log.d(TAG , TAGSMS + " EXCEPTION " + e.getMessage() + " | " + e.toString());
			}
		} else if (ContactsContract.Contacts.CONTENT_URI.toString().compareTo(content)==0) {
			// not easy to catch a modification in contact list
			Log.d(TAG , TAGCONTACT + " | contact modified... " );
			/*try{
				cur = contexto.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null,null, null, null);
				String _ID = ContactsContract.Contacts._ID;
				String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
				String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;
				Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
				String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
				String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;
				Uri EmailCONTENT_URI =  ContactsContract.CommonDataKinds.Email.CONTENT_URI;
				String EmailCONTACT_ID = ContactsContract.CommonDataKinds.Email.CONTACT_ID;
				String DATA = ContactsContract.CommonDataKinds.Email.DATA;

				StringBuffer output = new StringBuffer();
				String phoneNumber = null;
				String email = null;
				
				while (cur.moveToNext()) {
					String contact_id = cur.getString(cur.getColumnIndex( _ID ));
					String name = cur.getString(cur.getColumnIndex( DISPLAY_NAME ));
					int hasPhoneNumber = Integer.parseInt(cur.getString(cur.getColumnIndex( HAS_PHONE_NUMBER ))); 
					if (hasPhoneNumber > 0) {
						Log.d(TAG ,"\n First Name:" + name);
						// Query and loop for every phone number of the contact
	                    Cursor phoneCursor = contexto.getContentResolver().query(PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?", new String[] { contact_id }, null);
	                    while (phoneCursor.moveToNext()) {
	                        phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER));
	                        Log.d(TAG , "\n Phone number:" + phoneNumber);
	                    }
	                    phoneCursor.close();
	                    // Query and loop for every email of the contact
	                    Cursor emailCursor = contexto.getContentResolver().query(EmailCONTENT_URI,    null, EmailCONTACT_ID+ " = ?", new String[] { contact_id }, null);
	                    while (emailCursor.moveToNext()) {
	                        email = emailCursor.getString(emailCursor.getColumnIndex(DATA));
	                        Log.d(TAG ,"\nEmail:" + email);
	                    }
	                    emailCursor.close();
	                }
	            }
			}catch(Exception e){
				Log.d(TAG , "EXCP " + e.toString());
			}*/
		} else if ("???".equals(content)) {
			Log.d(TAG , " ... " + content);
		} else if ("???".equals(content)) {
			Log.d(TAG , " ... " + content);
		}

	}
}