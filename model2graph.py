################################################################################
# (c) 2013, COmputer SEcurity (COSEC) - Universidad Carlos III de Madrid
# Author: Guillermo Suarez de Tangil - guillermo.suarez.tangil@uc3m.es
################################################################################

import sys, json, time, curses, signal, os
import zipfile, StringIO
import redis
import markov
from markov import Markov



# -----------------------
# Logo
# -----------------------
 

print '##########################################################################'
print '# Author: Guillermo Suarez de Tangil - guillermo.suarez.tangil@uc3m.es   #'
print '# (c) 2013, COmputer SEcurity (COSEC) - Universidad Carlos III de Madrid #'
print '##########################################################################'
print '#                                                                        #'
print '#                                                                        #'
print '#                                                                        #'
print '#   .d8888b .d88b.  .d8888b   .d88b.   .d8888b                           #'
print '#  d88P"   d88""88b 88K      d8P  Y8b d88P"                              #'
print '#  888     888  888 "Y8888b. 88888888 888                                #'
print '#  Y88b.   Y88..88P      X88 Y8b.     Y88b.                              #'
print '#   "Y8888P "Y88P"   88888P\'  "Y8888   "Y8888P                           #'
print '#                                                                        #'
print '#                                                                        #'
print '#                                                                        #'
print '#                    .d8888b.                                            #'
print '#                   d88P  Y88b                                           #'
print '#                        .d88P                                           #'
print '#  888  888  .d8888b    8888"  88888b.d88b.       .d88b.  .d8888b        #'
print '#  888  888 d88P"        "Y8b. 888 "888 "88b     d8P  Y8b 88K            #'
print '#  888  888 888     888    888 888  888  888     88888888 "Y8888b.       #'
print '#  Y88b 888 Y88b.   Y88b  d88P 888  888  888 d8b Y8b.          X88       #'
print '#   "Y88888  "Y8888P "Y8888P"  888  888  888 Y8P  "Y8888   88888P\'       #'
print '#                                                                        #'
print '#                                                                        #'
print '#                                                                        #'
print '##########################################################################'
print '########################### COSEC Model2Graph ############################'
print '##########################################################################'


# -----------------------
# Variables
# -----------------------

model_name = ""
if len(sys.argv) >= 2 and len(sys.argv[1]) > 0:
    model_name = sys.argv[1]
else:
    sys.exit(1)

options = ""
if len(sys.argv) >= 3 and len(sys.argv[2]) > 0:
    options = sys.argv[2]

# -----------------------
# Methors
# -----------------------


def print_all_scores(model, all_scores):
    for s in all_scores:
        print s + ": " + str(model.score_for_pair([s])) #+ " / " + max_for_key(b, model.client)

def print_all_pairs(model, keys):
    print "digraph " + model.prefix + "{"
    for b1 in keys:
        for b2 in keys:
            #if model.score_for_pair([b1]) == 0 and model.score_for_pair([b2]):
            #    continue
            pair = [b1, b2]
            score = model.score_for_pair(pair)
            if score > 0:
                print b1.replace("-", "") + " -> " + b2.replace("-", "") + " [ label =  " + "%.2f" % (score) + " ]"
    print "}"

def get_keys(model):
    model_keys = model.get_keys()
    return [k.split(":")[1] for k in model_keys]

# -----------------------
# Main
# -----------------------

print "# Using model: " + model_name

model =  Markov(prefix=model_name)
keys = get_keys(model)


if "-" in options and "k" in options:
    print keys

if "-" in options and "g" in options:
    print "# " + str(keys)
    print_all_pairs(model, keys)



