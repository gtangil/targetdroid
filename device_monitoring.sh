#!/usr/bin/env bash

#LOGCAT_PARAMS="dalvikvm:W OSNetworkSystem:W *:S"
#LOGCAT_PARAMS="-b radio"
#LOGCAT_PARAMS="-b events"
#LOGCAT_PARAMS="-v raw dalvikvm:W OSNetworkSystem:W"
#LOGCAT_PARAMS="dalvikvm:W OSNetworkSystem:W"

MI_LOGCAT_PARAMS=""

adb -d logcat -c | adb -d logcat $LOGCAT_PARAMS | tee adb_logcat_device.log | python device2clone.py $1
