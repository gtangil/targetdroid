################################################################################
# (c) 2013, COmputer SEcurity (COSEC) - Universidad Carlos III de Madrid
# Author: Guillermo Suarez de Tangil - guillermo.suarez.tangil@uc3m.es
################################################################################

import sys
from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice

#Template

clone = MonkeyRunner.waitForConnection(deviceId='emulator-5554')
runAction = 'com.android.launcher.action.launcheraction'
runFlags = 0x10000000
runComponent = 'com.android.launcher/.CustomShirtcutActivity'
clone.startActivity(action=runAction, flags=runFlags, component=runComponent)
