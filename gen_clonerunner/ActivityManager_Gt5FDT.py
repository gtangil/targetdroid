################################################################################
# (c) 2013, COmputer SEcurity (COSEC) - Universidad Carlos III de Madrid
# Author: Guillermo Suarez de Tangil - guillermo.suarez.tangil@uc3m.es
################################################################################

import sys
from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice

#Template

clone = MonkeyRunner.waitForConnection(deviceId='device')
runAction = 'android.intent.action.MAIN'
runCategories = ['android.intent.category.HOME']
runFlags = 0x10000000
runComponent = 'com.android.launcher/.Launcher'
clone.startActivity(action=runAction, categories=runCategories, flags=runFlags, component=runComponent)
