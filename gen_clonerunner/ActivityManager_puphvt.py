################################################################################
# (c) 2013, COmputer SEcurity (COSEC) - Universidad Carlos III de Madrid
# Author: Guillermo Suarez de Tangil - guillermo.suarez.tangil@uc3m.es
################################################################################

import sys
from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice

#Template

clone = MonkeyRunner.waitForConnection(deviceId='emulator-5554')
runAction = 'android.intent.action.MAIN'
runCategories = ['android.intent.category.LAUNCHER']
runFlags = 0x10200000
runComponent = 'com.android.contacts/.DialtactsActivity'
clone.startActivity(action=runAction, categories=runCategories, flags=runFlags, component=runComponent)
