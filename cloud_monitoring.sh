#!/usr/bin/env bash

#LOGCAT_PARAMS="dalvikvm:W OSNetworkSystem:W *:S"
#LOGCAT_PARAMS="-b radio"
#LOGCAT_PARAMS="-b events"
#LOGCAT_PARAMS="-v raw dalvikvm:W OSNetworkSystem:W"
LOGCAT_PARAMS="dalvikvm:W OSNetworkSystem:W"

adb -e logcat -c | adb -e logcat $LOGCAT_PARAMS | tee adb_logcat_cloud.log | python cloud2behaviour.py $1
