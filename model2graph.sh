#!/usr/bin/env bash

python model2graph.py $1 -k
python model2graph.py $1 -g  | tee graphs/$1.gv | dot -Tpdf > graphs/$1.pdf

#python model2graph.py $1 -g > graphs/$1.gv  
#dot -Tpdf -Gcenter -Gsize="6,6" graphs/$1.gv -o  graphs/$1.pdf

echo "Output: graphs/$1.pdf"