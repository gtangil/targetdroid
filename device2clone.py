################################################################################
# (c) 2013, COmputer SEcurity (COSEC) - Universidad Carlos III de Madrid
# Author: Guillermo Suarez de Tangil - guillermo.suarez.tangil@uc3m.es
################################################################################

import sys, json, time, curses, signal, os
import zipfile, StringIO
from threading import Thread
from xml.dom import minidom
#from utils import AXMLPrinter
import hashlib
from pylab import *
from unicodedata import normalize

from device2cloneEvents.LogcatEvent import *

# -----------------------
# Constants
# -----------------------
debug = 1

EVENTENABLEDLIST = ["ActivityManager", "PackageManager", "AirplaneModeEnabler", "CAPTUREVENTS", "MmsSmsProvider", "GpsLocationProvider", "ConnectivityService", "PowerManagerService"];


# -----------------------
# Functions
# -----------------------

def normalize_string(unicode_string):
    return normalize('NFKD',unicode_string).encode('ascii', 'ignore')

def interruptHandler(signum, frame):
    raise KeyboardInterrupt

def decode(s, encodings=('ascii', 'utf8', 'latin1')):
    for encoding in encodings:
        try:
            return s.decode(encoding)
        except UnicodeDecodeError:
            pass
    return s.decode('ascii', 'ignore')


# -----------------------
# Params
# -----------------------

duration = 0
if len(sys.argv) >= 2 and len(sys.argv[1]) > 0:
    try:
        duration = int(sys.argv[1])
    except:
	    print "Usage: ./device_monitoring.sh [seconds]"
	    sys.exit(1)
	    
# -----------------------
# Logo
# -----------------------

print '##########################################################################'
print '# Author: Guillermo Suarez de Tangil - guillermo.suarez.tangil@uc3m.es   #'
print '# (c) 2013, COmputer SEcurity (COSEC) - Universidad Carlos III de Madrid #'
print '##########################################################################'
print '#                                                                        #'
print '#                                                                        #'
print '#                                                                        #'
print '#   .d8888b .d88b.  .d8888b   .d88b.   .d8888b                           #'
print '#  d88P"   d88""88b 88K      d8P  Y8b d88P"                              #'
print '#  888     888  888 "Y8888b. 88888888 888                                #'
print '#  Y88b.   Y88..88P      X88 Y8b.     Y88b.                              #'
print '#   "Y8888P "Y88P"   88888P\'  "Y8888   "Y8888P                           #'
print '#                                                                        #'
print '#                                                                        #'
print '#                                                                        #'
print '#                    .d8888b.                                            #'
print '#                   d88P  Y88b                                           #'
print '#                        .d88P                                           #'
print '#  888  888  .d8888b    8888"  88888b.d88b.       .d88b.  .d8888b        #'
print '#  888  888 d88P"        "Y8b. 888 "888 "88b     d8P  Y8b 88K            #'
print '#  888  888 888     888    888 888  888  888     88888888 "Y8888b.       #'
print '#  Y88b 888 Y88b.   Y88b  d88P 888  888  888 d8b Y8b.          X88       #'
print '#   "Y88888  "Y8888P "Y8888P"  888  888  888 Y8P  "Y8888   88888P\'       #'
print '#                                                                        #'
print '#                                                                        #'
print '#                                                                        #'
print '##########################################################################'
print '########################### COSEC Device2Clone ###########################'
print '##########################################################################'


# -----------------------
# Main
# -----------------------

call(['adb', 'devices'], stderr=PIPE)
p = Popen(["adb", "devices"], stdout=PIPE)
out, err = p.communicate()
for device in out.split():
    if "emulator" in device:
        device = device.split()[0]
        break
    else:
        assert Exception('Clone is not up!')

print "Using clone at: " + device

if duration:
    print "Running duration:", duration, "seconds"
    signal.signal(signal.SIGALRM, interruptHandler)
    signal.alarm(duration)
while 1:
    try:
        logcatInput = sys.stdin.readline().strip()
        if not logcatInput:
            break
        
        inputDecoded = normalize_string(logcatInput.decode("utf-8", "replace"))
        print decode(inputDecoded)

        try:
            if(len(inputDecoded.split('(')[0][2:].strip()) > 0):
                event = Event(inputDecoded)
                cloneEvent = event.get_event_handler(device)
                print "--------" + cloneEvent.event_type() + "--------"
                if (str(event.event_type) in EVENTENABLEDLIST):
                    cloneEvent.clone_run()

        except EventNotSupportedByCloud:
            pass

    except KeyboardInterrupt:
        break
