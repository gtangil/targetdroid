
#!/usr/bin/env python
###############################################################################################
# (c) 2014, COmputer SEcurity (COSEC) - Universidad Carlos III de Madrid. All rights reserverd.
# Main Author: Guillermo Suarez de Tangil - guillermo.suarez.tangil@uc3m.es
###############################################################################################

import sys, os, inspect, signal, StringIO
#re, cmd, code, re
from time import sleep
from subprocess import Popen, PIPE, STDOUT, call
import threading
from threading import Thread
import multiprocessing
from multiprocessing import Process

from tempfile import mkstemp
from shutil import move, rmtree
from os import remove, close

#try:
#    from subprocess import SubprocessError, TimeoutExpired
#except ImportError:
#    print "You need Python 3.3"
#    raise

from optparse import OptionParser

import droidbox
from droidbox.cosec_droidbox import *


# PyDev sets PYTHONPATH, use it
try:
    for p in os.environ['PYTHONPATH'].split(':'):
        if not p in sys.path:
            sys.path.append(p)
except:
    pass



version = "0.1 UC3M-Tennessee release"

option_0 = { 'name' : ('-i', '--input'), 'help' : 'file : inject faults in this apk', 'nargs' : 1 }
option_1 = { 'name' : ('-v', '--version'), 'help' : 'version of the API', 'action' : 'count' }
option_2 = { 'name' : ('-d', '--directory'), 'help' : 'directory : use this directory, test all apks', 'nargs' : 1 }
option_3 = { 'name' : ('-t', '--time'), 'help' : 'time : time of the dynamic analysis', 'nargs' : 1 }
option_p = { 'name' : ('-p', '--port'), 'help' : 'port : starting pool Android emulator port', 'nargs' : 1 }

options = [option_0, option_1, option_2, option_3, option_p]

cosec_prefix = 'cosec'

'''
    The console port number must be an even integer between 5554 and 5584,
    inclusive.
'''

STARTING_POOL_PORT = 5554


# -----------------------
# Logo
# -----------------------

cosec_logo = '''
##########################################################################
# Author: Guillermo Suarez de Tangil - guillermo.suarez.tangil@uc3m.es   #
# (c) 2014, COmputer SEcurity (COSEC) - Universidad Carlos III de Madrid #
##########################################################################
#                                                                        #
#                                                                        #
#                                                                        #
#   .d8888b .d88b.  .d8888b   .d88b.   .d8888b                           #
#  d88P"   d88""88b 88K      d8P  Y8b d88P"                              #
#  888     888  888 "Y8888b. 88888888 888                                #
#  Y88b.   Y88..88P      X88 Y8b.     Y88b.                              #
#   "Y8888P "Y88P"   88888P\'  "Y8888   "Y8888P                           #
#                                                                        #
#                                                                        #
#                                                                        #
#                    .d8888b.                                            #
#                   d88P  Y88b                                           #
#                        .d88P                                           #
#  888  888  .d8888b    8888"  88888b.d88b.       .d88b.  .d8888b        #
#  888  888 d88P"        "Y8b. 888 "888 "88b     d8P  Y8b 88K            #
#  888  888 888     888    888 888  888  888     88888888 "Y8888b.       #
#  Y88b 888 Y88b.   Y88b  d88P 888  888  888 d8b Y8b.          X88       #
#   "Y88888  "Y8888P "Y8888P"  888  888  888 Y8P  "Y8888   88888P\'       #
#                                                                        #
#                                                                        #
#                                                                        #
##########################################################################
###################### COSEC Android Testing Env #########################
##########################################################################
'''
print cosec_logo


# -----------------------
# Cosec Android Testing Env
# -----------------------

def replace(file_path, pattern, subst):
    #Create temp file
    fh, abs_path = mkstemp()
    new_file = open(abs_path,'w')
    old_file = open(file_path)
    for line in old_file:
        new_file.write(line.replace(pattern, subst))
    #close temp file and replace it for the original one
    new_file.close()
    close(fh)
    old_file.close()
    remove(file_path)
    move(abs_path, file_path)

def get_emulator_name(port='5554'):
    emulator = 'emulator-' + port
    return emulator

def get_port():
    poolWorkerName = multiprocessing.current_process().name
    if "MainProcess" in poolWorkerName:
        return str(STARTING_POOL_PORT)
    else:
        strID=poolWorkerName[poolWorkerName.index('-')+1:]
        poolWorkerID = int(strID)
        #<port>+1 must be free and will be reserved for ADB.
        return str(STARTING_POOL_PORT + poolWorkerID*2)

def init_sandbox(port='5554'):
    stop_and_delete_sandbox(port)
    create_and_start_sandbox(port)
    # set the interrupt handler to stop the emulator after a signal...
    signal.signal(signal.SIGALRM, interruptHandler)
    signal.signal(signal.SIGINT, interruptHandler)

def stop_and_delete_sandbox(port):
    emulator = get_emulator_name(port)
    # Delete previous emulators (paranoid mode)
    call(['adb', '-s', emulator, 'emu', 'kill'])
    call(['android', '-s', 'delete', 'avd', '-n', cosec_prefix + '-' + emulator])
    call(['rm', '-r', 'imagesDroidboxTmp-' + emulator])
    print "> Wiped sandbox... ", cosec_prefix + '-' + emulator
    signal.alarm(0)

def create_and_start_sandbox(port, target10Not16=True):
    # Instatiating our sandbox
    emulator = get_emulator_name(port)
    imagesDroidboxTmp = 'imagesDroidboxTmp-' + emulator
    print "> Initiating our sandbox... ", emulator
    
    # Create new emulators and images
    # TODO: Print an error if user does not have 'android-10'... check: android list
    abi = 'armeabi' #'armeabi' #'x86'
    
    if target10Not16:
        p = Popen(['android', '-s', 'create', 'avd', '-n', cosec_prefix + '-' + emulator, '-f', '-c', '100M', '-t',  'android-10', '-b', abi], stdin=PIPE)
        print p.communicate(input='no\n')[0]
        imagesDroidbox = 'imagesDroidbox23'
        call(['cp', '-r', imagesDroidbox, imagesDroidboxTmp])
        #--Run custom kernel --
        Popen (['emulator', '-avd', cosec_prefix + '-' + emulator, '-system', imagesDroidboxTmp + '/system.img', '-kernel', imagesDroidboxTmp + '/zImage', '-wipe-data', '-prop', 'dalvik.vm.execution-mode=int:portable', '-port', port, '-no-boot-anim', '-no-window', '-noaudio'])
        #--Run default kernel --
        #Popen (['emulator', '-avd', cosec_prefix + '-' + emulator, '-system', imagesDroidboxTmp + '/system.img', '-wipe-data', '-prop', 'dalvik.vm.execution-mode=int:portable', '-port', port])
    else:
        p = Popen(['android', '-s', 'create', 'avd', '-n', cosec_prefix + '-' + emulator, '-f', '-c', '100M', '-t',  'android-16', '-b', abi], stdin=PIPE)
        print p.communicate(input='no\n')[0]
        imagesDroidbox = 'imagesDroidbox41'
        call(['cp', '-r', imagesDroidbox, imagesDroidboxTmp])
        print "> updating RAM of the emulator..."
        alterdoid_ini = "~/.android/avd/" + cosec_prefix + "-" + emulator + ".avd/config.ini"
        replace(os.path.expanduser(alterdoid_ini), "hw.heapSize=48", "hw.heapSize=128MB")
        replace(os.path.expanduser(alterdoid_ini), "hw.ramSize=512", "hw.ramSize=700MB")
        Popen (['emulator', '-avd', cosec_prefix + '-' + emulator, '-system', imagesDroidboxTmp + '/system.img', '-ramdisk', imagesDroidboxTmp + '/ramdisk.img', '-data', imagesDroidboxTmp + '/userdata-qemu.img', '-kernel', imagesDroidboxTmp + '/kernel-qemu', '-wipe-data', '-prop', 'dalvik.vm.execution-mode=int:portable', '-port', port, '-no-boot-anim', '-no-window', '-noaudio'])
    
    print "> Created new execution evironment. Wating for emulator..."
    
    # Wait for emulator and clean logcat
    call(['adb', '-s', emulator, 'wait-for-device'])
    
    bootcomplete = None
    count = 0
    while bootcomplete is not 1:
        p = Popen(['adb', '-s', emulator, 'wait-for-device', 'shell', 'getprop', 'sys.boot_completed'], stdout=PIPE)
        out=p.communicate()
        try:
            bootcomplete = int(str(out[0]).strip())
        except:
            pass
        sleep(2)
        count+=1
        if count % 10 is 0:
            print "Waiting boot completed... ", bootcomplete

    call(['adb', '-s', emulator, 'logcat', '-c'])


def interruptHandler(signum, frame):
    print "> Timeout: stopping analysis"
    stop_and_delete_sandbox('5554') #TODO: Pass argument port
    

def timeout( p ):
    print "terminate..."
    if p.poll() == None:
        try:
            p.terminate()
            p.kill()
            stop_and_delete_sandbox()
        except:
            pass

'''
    Executes a dynimic analysis using Droidbox and ouptus the signature.
'''
def get_activity_signature(app, duration, port = None):
    
    # init sandbox if none one is given.
    # if one is give, should be initialized already.
    if port is None:
        port = get_port()
    init_sandbox(port)
    
    emulator = get_emulator_name(port)
    
    print "> Running analysis"
    call(['adb', '-s', emulator, 'logcat', '-c'])

    droidbox = DroidBox(app, 0)
    p = Popen(['adb', '-s', emulator, 'logcat', 'dalvikvm:W', 'OSNetworkSystem:W'], stdout=PIPE)
    
    # Stop the emulator after a duration...
    if duration and duration > 0:
        sleep(duration)
        stop_and_delete_sandbox(port)
        #signal.alarm(duration)
        # This should be reduntant than signal.alarm
        #t = threading.Timer( duration, timeout, [p] )
        #t.start()
        #t.join()
    
    #... meanwhile read the output of the sandbox (until the pipe is consumed)
    logcat, logcatErr = p.communicate()

    # When done: stop and process the output
    try:
        p.terminate()
        p.kill()
    except:
        pass

    logcatIO = StringIO.StringIO(logcat)
    signature = droidbox.run(logcatIO)
    print "\t - SIG[" + app + "]:", signature

    return signature


def get_summary_execution(file_name, inittime, endtime, signature_original):
    out = "=============================" + "\n"
    out += "[SAMPLE] " + file_name + "\n"
    out += endtime + "\n"
    out += "[ORIGINAL SIGNATURE] " + str(signature_original) + "\n"
    out += "\n"
    return out

def cosecTest(file_name, duration):
    # .............................
    #init_sandbox()
    # .............................
    #get_activity_signature(options.input, duration)
    # .............................
    #test_view_clientDump(options.input)
    # .............................
    #file_test = "droidbox/DroidBoxTests.apk"
    #signature = get_activity_signature(file_test, 2*60)
    #print signature
    # .............................
    #file_test = "samples/GingerMaster/2e9b8a7a149fcb6bd2367ac36e98a904d4c5e482.apk"
    #signature = get_activity_signature(file_test, 0)
    #print signature
    # .............................
    print "End tests"


def error_deleting(func, path, excinfo):
    error = open('error.txt', 'a+')
    error.write("[ERROR RM] " + str(excinfo) + " at " + str(path)  + "\n")
    error.close()

'''
    Controller
'''
def cosec(file_name, duration):
    
    print " -------------------------------------- "
    print file_name, ":"
    
    try:
        
        # -- report results
        report = open('report.txt', 'a')
        history = open('history.txt', 'a')
        history.write(file_name + '\n')
        history.close()
        
        '''
            Dynamic Analisys ---------------------------------------
        '''
        inittime = time.time()
        signature_original = []
        signature_modified = {}
        
        if os.path.isfile(file_name):
            signature_original = get_activity_signature(file_name, duration)
        enddynamic = time.time()
        endtime = "[DURATION] Dynamic: " + str(enddynamic-inittime)
        print endtime
        
        
        # -- report results
        out = get_summary_execution(file_name, inittime, endtime, signature_original)
        if len(signature_original) is 0:
            print "No signatures found for this app!"
            report.write(out)
            report.write("\n\n")
            report.close()
            return

        report.write(out)
        report.write("\n\n")
        report.close()
        return len(signature_original)
    
    except Exception, e :
        error = open('error.txt', 'a+')
        error.write('[ERROR COSEC]' + str(e) + '\n')
        error.close()
        print "ERROR", e
        import traceback
        traceback.print_exc()
        try:
            report.close()
            reportSample.close()
        except:
            pass



def main(options, arguments) :
    
    if options.port != None:
        STARTING_POOL_PORT = int(options.port)

    if options.time == None:
        duration = 0
    else:
        duration = int(options.time)


    if options.input != None :
        cosec(options.input, duration)
        #!#cosecTest(options.input, duration)

    elif options.directory != None :
        for root, dirs, files in os.walk( options.directory, followlinks=True ):
            # -- Get real path to valid apks
            real_filenames = []
            if files != [] :
                for f in files :
                    real_filename = root
                    if real_filename[-1] != "/" :
                        real_filename += "/"
                    real_filename += f
                    real_filenames.append(real_filename)

            try:
                history = open('history.txt')
                history_filenames = [line.strip('\n') for line in history]
                try:
                    history_filenames.remove('')
                except ValueError:
                    pass
                history.close()
            except IOError:
                history_filenames = []

            try:
                num_cpus = multiprocessing.cpu_count()
            except NotImplementedError:
                num_cpus = 2
            
            # -- Create pool of process:
            ''' 
                NOTE: The emulator console port number must be an even integer between 5554 and 5584, inclusive. <port>+1 must also be free and will be reserved for ADB. Thus, we can only parallize 15 emulators for now. This could probably be improved using adb forward or something similar.
            '''
            STARTING_POOL_PORT = 5552
            PROCESSES = 15 #int(num_cpus/2) + 1
            print 'Creating pool with %d processes\n' % PROCESSES
            pool = multiprocessing.Pool(PROCESSES) #, maxtasksperchild=100

            # -- Create tasks from all apks
            TASKS = [(f, duration) for f in set(real_filenames).symmetric_difference(history_filenames)]

            # -- Process all tasks
            results = [pool.apply_async(cosec, t) for t in TASKS]
            
            # -- Print results... equivalent than pool.apply as blocks the task
            #for r in results:
                #print '\t', r.get(), '\n'
            pool.close()
            pool.join()

    elif options.version != None :
        print "Cosec Android Testing Env. version %s" % version

if __name__ == "__main__" :
    if len(sys.argv[:]) <= 1:
        print "Try with option -h"
        exit()
    parser = OptionParser()
    for option in options:
        param = option['name']
        del option['name']
        parser.add_option(*param, **option)

    options, arguments = parser.parse_args()
    #sys.argv[:] = arguments
    report = open('report.txt', 'w')
    report.write(cosec_logo)
    report.close()
    error = open('error.txt', 'w')
    error.close()
    main(options, arguments)
