
Targetdroid
===========

We propose a system that addresses the problem of detecting targeted malware by relying on stochastic models of usage and context events derived from real user traces. By incorporating the behavioral particularities of a given user, our scheme provides a solution for detecting malware targeting such a specific user. In this implementation, we provide an evidence generation module. This module extracts usage and context traces from the device and generates the stochastic triggering model by first cloning the user device in the cloud and then injecting the triggering patterns over the clone.

The testing environment has been set up using a smartphone and a virtual mobile device in the cloud, both running Android OS 2.3. In particular, a Google Nexus One is used for the physical device and an Android emulator for the clones. The device is instrumented with various monitoring tools that collect user events, the context, and the device  configuration and transmits them to the cloud. For this purpose, we used a combination of logcat and getevent tools from the Android Debug Bridge (ADB). This implementation builds on a number of previous works for cloud cloning smartphone platforms and for performing behavioral analysis. In the cloud end, a middleware implemented in Python processes all inputs received, generates the associated models, and runs the simulation. We inject events and contexts into apps with a combination of a testing tool called Monkeyrunner and the Android emulator console.

As for the behavioral signatures obtained in the virtual device, we have used an open source dynamic analysis tool called Droidbox to monitor various activities that can be used to characterize app behavior and tell apart benign from suspicious behavior. Droidbox is based on TaintDroid and provides a variety of data about how an app is behaving. We chose 20 relevant activities to characterize app behavior, which include information about calls to the crypto API (cryptousage), I/O network and file activity (opennet, sendnet, accessedfiles, etc.), phone and SMS activity (phonecalls, sendsms), data exfiltration through the network (dataleak), and dynamic code injection (dexclass), among others.


Version
-------
 
1.0


Tech
-----------

Alterdroid uses the following dependenices:

 - Ubuntu Server or Desktop (it can be ported to Windows and Mac as well). 
 - Python 2.7
 - Library Markov python.
 - Library networkx-1.7.
 - Library PyYAML-3.10.
 - Library redis-2.6.16
 - Dot Graph


Install Android
--------------

wget http://dl.google.com/android/android-sdk_r22.6-linux.tgz
tar -xvzf android-sdk_r20-linux.tgz
cd ~/android-sdk-linux/tools
android update sdk --no-ui
vim ~/.bashrc
export PATH=${PATH}:~/android-sdk-linux/tools 
export PATH=${PATH}:~/android-sdk-linux/platform-tools
sudo apt-get install libc6:i386 libstdc++6:i386 ia32-libs
android update sdk --filter android-10 --no-ui 


Install Markov 
--------------

sudo apt-get install python-markov

Main Components
--------------
 - device2Cloud: Clone events generated in a physical device (connected through adb usb/ip)  to the cloud. This component is accessed via device_monitoring.sh. 
 - cloud2Behaviour: This component is accessed via cloud_monitoring.sh.


Execution
--------------

1 Sturt up the cloud:

Usage: ./startupcloud.sh [Target device]

2	Sturt up the database:

Usage: ./redis-server

3	Device to cloud:

Usage: ./device_monitoring.sh [duration in seconds]

4	Cloud to monitoring:

Usage: ./cloud_monitoring.sh [duration in seconds]

5	Device/Cloud to log:

Usage: ./device2usage.sh 

6	Context to Model:

Usage: ./contex2model.sh context map_file [delete]

context: path to the file containing the context
map_file: path to the file containing the map context-event
delete: flush previous model

7	Model to cloud:

Usage: ./model2cloud.sh model_name [duration] 
model_name: name of the model stored in the database
duration: duration in seconds

8	Model to graph:

Usage: ./model2graph.sh model_name [options] 
model_name: name of the model stored in the database
options: [-p] [-k] [-g]  
	-k: print keys
	-g: print keys and graph


Examples
--------------

# Start up the Cloud
./startupcloud.sh android23

# Start up the database
cd libs/ redis-2.6.16/src/
./redis-server

# Device to clone
./device_monitoring.sh 120

# Cloud to behaviour
./device_monitoring.sh 120

# Context to model:
./libs/ redis-2.6.16/src/redis-server
./context2model.sh adb_logcat_device_context.log context2model.map True

# Model to cloud
./libs/ redis-2.6.16/src/redis-server
./model2cloud.sh markovChain 60

# Model to graph
./libs/ redis-2.6.16/src/redis-server
./model2graph.sh markovChain -g

