################################################################################
# (c) 2013, COmputer SEcurity (COSEC) - Universidad Carlos III de Madrid
# Author: Guillermo Suarez de Tangil - guillermo.suarez.tangil@uc3m.es
################################################################################

import sys, json, time, curses, signal, os
import zipfile, StringIO
from threading import Thread
from xml.dom import minidom
#from utils import AXMLPrinter
import hashlib
import redis
import unittest
import markov
from markov.markov import Markov


# -----------------------
# Constants
# -----------------------
debug = 1

# -----------------------
# Functions
# -----------------------

# From DroidBox - http://code.google.com/p/droidbox/
def decode(s, encodings=('ascii', 'utf8', 'latin1')):
    for encoding in encodings:
        try:
            return s.decode(encoding)
        except UnicodeDecodeError:
            pass
    return s.decode('ascii', 'ignore')

# -----------------------
# Params
# -----------------------

	    
# -----------------------
# Logo
# -----------------------

print "COSEC Events2Model"   

# -----------------------
# Variables
# -----------------------

regular = False

model =  Markov(prefix="events")
#model =  Markov(prefix="eventsTrim2")
#model =  Markov(prefix="eventsTrim2Regular")
model.flush(prefix=True)

buffer_max_len = 2
buffer = []
all = []

# -----------------------
# Main
# -----------------------
while 1:
    try:
        logInput = sys.stdin.readline()
        if not logInput:
            break
       
        # Decode input events and checking if they are supported
        event_raw = decode(logInput)
                        
        # Parsing input events
        elements = event_raw.split(': ')
        event = str(elements[2]).strip()
            
        if len(event)>0:
            event = event.replace(" ", "_")
        
        if len(event)<=0:
            continue
                
        print(event)

        try:
            all.index(event)
        except ValueError:
            all.append(event)
                
        # Add the events to the model
        buffer.append(event)
            
        # Add the events to the model
        if len(buffer) > buffer_max_len:
            #buffer_line = ' '.join([str(x) for x in buffer])
            model.add_pair_to_index(buffer)
            buffer = []

    except KeyboardInterrupt:
        break


# Add the reminder events to the model
if len(buffer) > 0:
    #buffer_line = ' '.join([str(x) for x in buffer])
    model.add_pair_to_index(buffer)

if regular :
    print "Making the model regular"
    # Making the model regular
    for i in range(len(all)):
        for j in range(len(all)):
            buffer.append(all[i])
            buffer.append(all[j])
            model.add_pair_to_index(buffer)
            buffer = []

prediction = model.generate(max_words=buffer_max_len)
print prediction
