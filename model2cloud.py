################################################################################
# (c) 2013, COmputer SEcurity (COSEC) - Universidad Carlos III de Madrid
# Author: Guillermo Suarez de Tangil - guillermo.suarez.tangil@uc3m.es
################################################################################

import sys, json, time, curses, signal, os
import zipfile, StringIO
import redis
import markov
from markov import Markov
import time
#from subprocess import Popen, call, PIPE
import telnetlib, socket

# -----------------------
# Logo
# -----------------------


print '##########################################################################'
print '# Author: Guillermo Suarez de Tangil - guillermo.suarez.tangil@uc3m.es   #'
print '# (c) 2013, COmputer SEcurity (COSEC) - Universidad Carlos III de Madrid #'
print '##########################################################################'
print '#                                                                        #'
print '#                                                                        #'
print '#                                                                        #'
print '#   .d8888b .d88b.  .d8888b   .d88b.   .d8888b                           #'
print '#  d88P"   d88""88b 88K      d8P  Y8b d88P"                              #'
print '#  888     888  888 "Y8888b. 88888888 888                                #'
print '#  Y88b.   Y88..88P      X88 Y8b.     Y88b.                              #'
print '#   "Y8888P "Y88P"   88888P\'  "Y8888   "Y8888P                           #'
print '#                                                                        #'
print '#                                                                        #'
print '#                                                                        #'
print '#                    .d8888b.                                            #'
print '#                   d88P  Y88b                                           #'
print '#                        .d88P                                           #'
print '#  888  888  .d8888b    8888"  88888b.d88b.       .d88b.  .d8888b        #'
print '#  888  888 d88P"        "Y8b. 888 "888 "88b     d8P  Y8b 88K            #'
print '#  888  888 888     888    888 888  888  888     88888888 "Y8888b.       #'
print '#  Y88b 888 Y88b.   Y88b  d88P 888  888  888 d8b Y8b.          X88       #'
print '#   "Y88888  "Y8888P "Y8888P"  888  888  888 Y8P  "Y8888   88888P\'       #'
print '#                                                                        #'
print '#                                                                        #'
print '#                                                                        #'
print '##########################################################################'
print '########################### COSEC Model2Cloud ############################'
print '##########################################################################'


# -----------------------
# Variables
# -----------------------

model_name = ""
if len(sys.argv) <= 1:
    print "Error: Model not found."
    sys.exit(1)
if len(sys.argv) > 1 and len(sys.argv[1]) > 0:
    model_name = sys.argv[1]

duration = 0
if len(sys.argv) > 2 and len(sys.argv[2]) > 0:
    try:
        duration = int(sys.argv[2])
    except:
	    print "Error: the duration paramter is not a number."
	    sys.exit(1)

delay = 1
ip = "localhost"
port = 5554

# -----------------------
# Methors
# -----------------------

def interruptHandler(signum, frame):
    raise KeyboardInterrupt

def get_keys(model):
    model_keys = model.get_keys()
    return [k.split(":")[1] for k in model_keys]

# -----------------------
# Main
# -----------------------

print "# Using model: " + model_name

if duration > 0:
    print "# Duration:", duration
    signal.signal(signal.SIGALRM, interruptHandler)
    signal.alarm(duration)

model =  Markov(prefix=model_name)
keys = get_keys(model)

#prediction = model.generate(max_words=2)

try:
    telnet = telnetlib.Telnet(ip, port)
except EOFError:
    print "Error: cannot connect to the cloud..."
    
while 1:
    try:
        prediction = model.generate(max_words=2)
    
        for event in prediction:
            print "Sending event to the cloud: " + event
            # read until it is ready to interact
            telnet.read_until("OK")
            telnet.write(event.replace("_", " ") + "\n")
            # waiting some delay
            time.sleep(delay)
    
    except socket.error:
        print "Error: cannot connect to the cloud..."
        break #sys.exit(-1)
    except KeyboardInterrupt:
        break

telnet.close()
